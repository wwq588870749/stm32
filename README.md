# 溶液检测系统

## 介绍
一个基于STM32F103C8T6的液体颜色检测系统，以GY-33颜色传感器作为检测器件的颜色检测系统。系统将传感器检测到的24位RGB真彩色数据，显示于OLED显示屏上，并通过ESP8266模块建立TCP网络链接，在手机屏幕上显示相应的颜色。因为能显示24位RGB真彩色的屏幕成本不低，干脆直接将数据传入到手机，使用手机屏幕显示。

GY -33是一款低成本颜色识别传感器模块。工作电压3-5v ，功耗小，体积小，安装方便。其工作原理是，照明LED 发光，照射到被测物体后，返回光经过滤镜检测RGB 的比例值，根据RGB 的比例值识别出颜色。




## 模块接线

1. GY33颜色传感器  --> STM32F103C8T6
```

   VCC   --> 3.3V

   GND 	 --> GND

   CT    --> PA10	USART1_RX

   DR    --> PA9	USART1_TX
```

2.  ESP8266  --> STM32F103C8T6
```

    VCC    --> 3.3V (模块接5V电压会过热损坏)
    
    GND    --> GND
    
    UTXD   --> PA3	USART2_RX
    
    URXD   --> PA2	USART2_TX
    
    CH_PD  --> 3.3V(高电平)
    
    RST    --> 悬空
    
```

3. OLED  --> STM32F103C8T6
```

    VCC   --> 3.3V
    
    GND   --> GND
    
    SCL   --> PB6	I2C1_SCL
    
    SDA   --> PB7	I2C1_SDA
```

## 使用说明

1. 这里ESP8266模块是作为热点，手机连接wifi与该模块通信，实现局域网内的数据传输，IP地址端口可根据需要修改。

2. android_project.7z 为安卓APP源码，解压缩后使用Android Studio 打开项目

3. APP开发使用的是Android9.0(Pie)系统，在Redmi Note 5上进行真机测试可以运行

4. rgb2.0.7为STM32工程，使用STM32CubeIDE1.1.0以及 stm32cubef1 1.80版的HAL库开发

5. GY -33使用串口连接，配置成波特率9600bps ，连续输出方式，可掉电保存设置，不需要计算RGB 值。**配置完成后再接入系统**。使用`GY -33/pc`目录下的软件可对GY -33模块进行设置并查看校准结果。

6. GY -33模块更多用法可根据`GY -33`目录下的数据手册进行修改。

   

## 系统实物图

<img src="image/NO1.jpg" style="zoom:33%;" />

## 手机APP界面

<img src="image/app.png" style="zoom:50%;" />

因为OLED只能显示采集到的数值，不能呈现真正的颜色，而能显示24位RGB真彩色的屏幕成本不低，干脆直接将数据传入到手机，使用手机屏幕显示，主要是还可以尝试一下ESP8266模块和Android Studio。就使用java写了一个非常简陋的APP，跟单片机进行通信并显示颜色。